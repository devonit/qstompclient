QSTOMPCLIENT_VERSION = 0.0.1

isEmpty(QSTOMPCLIENT_LIBRARY_TYPE) {
    QSTOMPCLIENT_LIBRARY_TYPE = shared
}

QT += network
QSTOMPCLIENT_INCLUDEPATH = $${PWD}/src
QSTOMPCLIENT_LIBS = -lqstompclient
contains(QSTOMPCLIENT_LIBRARY_TYPE, staticlib) {
    DEFINES += QSTOMPCLIENT_STATIC
} else {
    DEFINES += QSTOMPCLIENT_SHARED
    win32:QSTOMPCLIENT_LIBS = -lqstompclient1
}

isEmpty(PREFIX) {
    unix {
        PREFIX = /usr
    } else {
        PREFIX = $$[QT_INSTALL_PREFIX]
    }
}

isEmpty(LIBDIR) {
    LIBDIR = lib
}

