#include <QTextStream>
#include <QIODevice>
#include <QDebug>

#include "qstompframe.h"
#include "qstompframe_p.h"
#include "qabstractstompsocket.h"

class QAbstractStompSocketPrivate
{
    Q_DECLARE_PUBLIC(QAbstractStompSocket)

public:
    QAbstractStompSocketPrivate(QAbstractStompSocket *parent)
        : device(0),
          readingHeader(true),
          q_ptr(parent)
    {
    }

    static bool nextByteIsNull(QIODevice *device)
    {
        char buf[1];
        if (device->peek(buf, sizeof(buf)) == sizeof(buf))
            return (buf[0] == '\0');
        return false;
    }

    // private slots
    void _q_deviceReadyRead();

    QIODevice *device;
    bool readingHeader;
    QString headerBuffer;
    QByteArray bodyBuffer;
    QStompFrame currentFrame;

    QAbstractStompSocket * const q_ptr;
};

QAbstractStompSocket::QAbstractStompSocket(QObject *parent)
    : QObject(parent),
      d_ptr(new QAbstractStompSocketPrivate(this))
{
}

QAbstractStompSocket::QAbstractStompSocket(QIODevice *device, QObject *parent)
    : QObject(parent),
      d_ptr(new QAbstractStompSocketPrivate(this))
{
    setDevice(device);
}

QAbstractStompSocket::~QAbstractStompSocket()
{
}

QIODevice *QAbstractStompSocket::device() const
{
    Q_D(const QAbstractStompSocket);
    return d->device;
}

void QAbstractStompSocket::setDevice(QIODevice *device)
{
    Q_D(QAbstractStompSocket);
    d->device = device;
    connect(d->device, SIGNAL(readyRead()), this, SLOT(_q_deviceReadyRead()));
}

void QAbstractStompSocketPrivate::_q_deviceReadyRead()
{
    Q_Q(QAbstractStompSocket);
    QIODevice *socket = qobject_cast<QIODevice*>(q->sender());
    if (!socket) {
        qDebug() << Q_FUNC_INFO << "invalid socket";
        return;
    }

    forever {
        bool done = false;
        if (readingHeader) {
            while (!done && socket->canReadLine()) {
                // the spec requires that all header data be UTF-8
                QString headerData = QString::fromUtf8(socket->readLine());
                if (headerData == QLatin1String("\n\n") || headerData == QLatin1String("\n"))
                    done = true;
                else
                    headerBuffer.append(headerData);
            }

            if (!done)
                return;

            // parse header
            QStringList headerLines = headerBuffer.split('\n');
            headerBuffer.clear();
            headerLines.removeAll(QString());
            if (headerLines.isEmpty()) {
                // this was exposed in a test by having a message that had more
                // data than the content-length described. My guess is that its
                // fine to return here after clearing, as this was most likely
                // junk data
                continue;
            }

            if (qEnvironmentVariableIsSet("QSTOMPCLIENT_DEBUG"))
                qDebug() << headerLines;

            QString commandString = headerLines.takeFirst().trimmed();
            QStompFrame::Command command =
                QStompFramePrivate::commandFromString(commandString);
            if (command == QStompFrame::INVALID) {
                qDebug() << Q_FUNC_INFO << "invalid header: " << headerLines;
                return;
            }

            currentFrame = QStompFrame(command);
            foreach (QString line, headerLines) {
                QStringList values = line.split(':');
                if (values.size() < 2) {
                    qDebug() << Q_FUNC_INFO << "invalid header: " << line;
                } else {
                    currentFrame.setRawHeader(values[0].trimmed(), values[1].trimmed());
                }
            }

            if (currentFrame.hasHeader(QStompFrame::ContentLengthHeader)) {
                int contentLength =
                    currentFrame.header(QStompFrame::ContentLengthHeader).toInt();
                bodyBuffer.resize(contentLength);
            }

            readingHeader = false;
            done = false;
        }

        // otherwise read body data
        if (currentFrame.hasHeader(QStompFrame::ContentLengthHeader)) {
            int contentLength =
                currentFrame.header(QStompFrame::ContentLengthHeader).toInt();
            qint64 bytesRead = socket->read(bodyBuffer.data(), contentLength);
            if (bytesRead < contentLength)
                return;
        } else {
            while (!done && socket->canReadLine()) {
                QByteArray bodyData = socket->readLine();
                int nullIdx = bodyData.indexOf('\0');
                if (nullIdx == -1) {
                    bodyBuffer.append(bodyData);
                } else {
                    bodyBuffer.append(bodyData.left(nullIdx));
                    done = true;
                }
            }

            // some frames have no body
            if (nextByteIsNull(socket)) {
                socket->getChar(0);  // ditch it
                done = true;
            }

            if (!done)
                return;
        }

        currentFrame.d->body = bodyBuffer;
        readingHeader = true;
        bodyBuffer.clear();

        // pass the frame along
        q->handleFrame(currentFrame);
    }
}

// NOTE: use QTextStream since STOMP expects UTF8 anyway. We might
// run into encoding issues in the future, but this is easy for now.
void QAbstractStompSocket::writeFrame(const QStompFrame &frame)
{
    if (!frame.isValid()) {
        qWarning() << Q_FUNC_INFO << "invalid frame";
        return;
    }

    Q_D(QAbstractStompSocket);
    QTextStream ts(d->device);
    ts.setCodec("UTF-8");
    ts << QStompFramePrivate::commandToString(frame.command()) << endl;
    QStompFramePrivate::RawStompHeaders::ConstIterator it =
            frame.d->rawHeaders.constBegin();
    QStompFramePrivate::RawStompHeaders::ConstIterator end =
            frame.d->rawHeaders.constEnd();
    for (; it != end; ++it)
        ts << (*it).first << ":" << (*it).second << endl;
    ts << endl;

    if (!frame.d->body.isEmpty())
        ts << frame.d->body << endl;

    ts << '\0' << endl;
}

#include "moc_qabstractstompsocket.cpp"
