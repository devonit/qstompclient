#ifndef QABSTRACTSTOMPSOCKET_P_H
#define QABSTRACTSTOMPSOCKET_P_H

#include <QObject>
#include "qstompframe.h"
#include "qstompclient_export.h"

class QIODevice;
class QStompSocketPrivate;
class QSTOMPCLIENT_EXPORT QAbstractStompSocket : public QObject
{
    Q_OBJECT
public:
    explicit QAbstractStompSocket(QObject *parent = 0);
    explicit QAbstractStompSocket(QIODevice *device, QObject *parent = 0);
    ~QAbstractStompSocket();

    QIODevice *device() const;
    void setDevice(QIODevice *device);

protected:
    virtual void handleFrame(const QStompFrame &frame) = 0;
    void writeFrame(const QStompFrame &frame);

private:
    Q_DISABLE_COPY(QAbstractStompSocket)
    Q_DECLARE_PRIVATE(QAbstractStompSocket)
    QScopedPointer<QAbstractStompSocketPrivate> d_ptr;

    Q_PRIVATE_SLOT(d_func(), void _q_deviceReadyRead())

};

#endif // QABSTRACTSTOMPSOCKET_H
