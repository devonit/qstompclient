#include <QStringList>
#include <QTcpSocket>
#include <QEventLoop>
#include <QTimer>

#include "qstompframe.h"
#include "qstompframe_p.h"
#include "qstompclient_p.h"
#include "qstompclient.h"

class QStompSubscriptionPrivate
{
public:
    QStompSubscriptionPrivate(QStompClient *parent)
        : client(parent)
    {
    }

    static QString acknowledgmentModeToString(QStompSubscription::AcknowledgmentMode mode);

    QStompClient *client;
    QString id;
    QString destination;
    QStompSubscription::AcknowledgmentMode mode;
};

QString QStompSubscriptionPrivate::acknowledgmentModeToString(QStompSubscription::AcknowledgmentMode mode)
{
    switch (mode) {
    case QStompSubscription::Client:
        return QLatin1String("client");
    case QStompSubscription::Auto:
        return QLatin1String("auto");
    case QStompSubscription::ClientIndividual:
        return QLatin1String("client-individual");
    }

    return QString();
}


QStompSubscription::QStompSubscription(const QString &id, const QString &destination,
                                       QStompSubscription::AcknowledgmentMode mode,
                                       QStompClient *parent)
    : d_ptr(new QStompSubscriptionPrivate(parent))
{
    Q_D(QStompSubscription);
    d->id = id;
    d->destination = destination;
    d->mode = mode;
}

QStompSubscription::~QStompSubscription()
{
}

QString QStompSubscription::id() const
{
    Q_D(const QStompSubscription);
    return d->id;
}

QString QStompSubscription::destination() const
{
    Q_D(const QStompSubscription);
    return d->destination;
}

QStompSubscription::AcknowledgmentMode QStompSubscription::acknowledgmentMode() const
{
    Q_D(const QStompSubscription);
    return d->mode;
}

void QStompSubscription::unsubscribe()
{
    Q_D(QStompSubscription);
    QStompFrame unsubFrame(QStompFrame::UNSUBSCRIBE);
    unsubFrame.setHeader(QStompFrame::IdHeader, d->id);
    d->client->writeFrame(unsubFrame);
}

QStompClientSpy::QStompClientSpy(QStompClient *client, QObject *parent)
    : QObject(parent)
{
    connect(client, SIGNAL(connected(QStompFrame)), this, SLOT(frameReceived(QStompFrame)));
    connect(client, SIGNAL(message(QStompFrame)), this, SLOT(frameReceived(QStompFrame)));
    connect(client, SIGNAL(error(QStompFrame)), this, SLOT(frameReceived(QStompFrame)));
    connect(client, SIGNAL(receipt(QString)), this, SLOT(receiptReceived(QString)));
    connect(client, SIGNAL(connected(QStompFrame)), &m_eventLoop, SLOT(quit()));
    connect(client, SIGNAL(message(QStompFrame)), &m_eventLoop, SLOT(quit()));
    connect(client, SIGNAL(error(QStompFrame)), &m_eventLoop, SLOT(quit()));
    connect(client, SIGNAL(receipt(QString)), &m_eventLoop, SLOT(quit()));
}

void QStompClientSpy::frameReceived(const QStompFrame &frame)
{
    enqueue(frame);
}

void QStompClientSpy::receiptReceived(const QString &receipt)
{
    m_receipts.enqueue(receipt);
}

bool QStompClientSpy::waitForAFrame(int msecs)
{
    int originalSize = size();
    QTimer::singleShot(msecs, &m_eventLoop, SLOT(quit()));
    m_eventLoop.exec();
    return size() > originalSize;
}

int QStompClientPrivate::s_subscriptionCounter = 0;
QStompClientPrivate::QStompClientPrivate(QStompClient *qq)
    : stompSocket(0),
      connected(false),
      incomingHeartbeatMs(-1),
      outgoingHeartbeatMs(-1),
      heartbeatTimer(0),
      disconnecting(false),
      q_ptr(qq)
{
}

QString QStompClientPrivate::commandName(QStompFrame::Command command) const
{
    switch (command) {
    case QStompFrame::SEND:
        return QLatin1String("SEND");
    case QStompFrame::SUBSCRIBE:
        return QLatin1String("SUBSCRIBE");
    case QStompFrame::UNSUBSCRIBE:
        return QLatin1String("UNSUBSCRIBE");
    case QStompFrame::BEGIN:
        return QLatin1String("BEGIN");
    case QStompFrame::COMMIT:
        return QLatin1String("COMMIT");
    case QStompFrame::ABORT:
        return QLatin1String("ABORT");
    case QStompFrame::ACK:
        return QLatin1String("ACK");
    case QStompFrame::NACK:
        return QLatin1String("NACK");
    case QStompFrame::DISCONNECT:
        return QLatin1String("DISCONNECT");
    case QStompFrame::CONNECT:
        return QLatin1String("CONNECT");
    case QStompFrame::STOMP:
        return QLatin1String("STOMP");
    case QStompFrame::CONNECTED:
        return QLatin1String("CONNECTED");
    case QStompFrame::MESSAGE:
        return QLatin1String("MESSAGE");
    case QStompFrame::RECEIPT:
        return QLatin1String("RECEIPT");
    case QStompFrame::ERROR:
        return QLatin1String("ERROR");
    case QStompFrame::INVALID:
        break;
    }

    return QString();
}

QStompClient::QStompClient(QObject *parent)
    : QAbstractStompSocket(parent),
      d_ptr(new QStompClientPrivate(this))
{
    Q_D(QStompClient);
    d->stompSocket = new QTcpSocket(this);
    connect(d->stompSocket, SIGNAL(connected()), this, SLOT(_q_socketConnected()));
    connect(d->stompSocket, SIGNAL(disconnected()), this, SLOT(_q_socketDisconnected()));
    connect(d->stompSocket, SIGNAL(error(QAbstractSocket::SocketError)),
                 this, SLOT(_q_socketError(QAbstractSocket::SocketError)));
    setDevice(d->stompSocket);
}

QStompClient::QStompClient(QIODevice *socket, QObject *parent)
    : QAbstractStompSocket(parent),
      d_ptr(new QStompClientPrivate(this))
{
    setDevice(socket);
}

QStompClient::~QStompClient()
{
}

QString QStompClient::login() const
{
    Q_D(const QStompClient);
    return d->login;
}

void QStompClient::setLogin(const QString &login)
{
    Q_D(QStompClient);
    if (d->connected) {
        qDebug() << Q_FUNC_INFO
                 << "already connected, this will have no effect until reconnect";
    }

    d->login = login;
}

QString QStompClient::passcode() const
{
    Q_D(const QStompClient);
    return d->passcode;
}

void QStompClient::setPasscode(const QString &passcode)
{
    Q_D(QStompClient);
    if (d->connected) {
        qDebug() << Q_FUNC_INFO
                 << "already connected, this will have no effect until reconnect";
    }

    d->passcode = passcode;
}

int QStompClient::outgoingHeartbeatMs() const
{
    Q_D(const QStompClient);
    return d->outgoingHeartbeatMs;
}

void QStompClient::setOutgoingHeartbeatMs(int msec)
{
    Q_D(QStompClient);
    if (d->connected) {
        qDebug() << Q_FUNC_INFO
                 << "already connected, this will have no effect until reconnect";
    }

    d->outgoingHeartbeatMs = msec;
}

int QStompClient::incomingHeartbeatMs() const
{
    Q_D(const QStompClient);
    return d->incomingHeartbeatMs;
}

void QStompClient::setIncomingHeartbeatMs(int msec)
{
    Q_D(QStompClient);
    if (d->connected) {
        qDebug() << Q_FUNC_INFO
                 << "already connected, this will have no effect until reconnect";
    }

    d->incomingHeartbeatMs = msec;
}

QByteArray QStompClient::session() const
{
    Q_D(const QStompClient);
    return d->session;
}

QStompSubscription *QStompClient::subscribe(const QString &destination,
    QStompSubscription::AcknowledgmentMode mode)
{
    Q_D(QStompClient);
    QStompClientPrivate::s_subscriptionCounter++;
    QString uniqueId = QString("qstompclient-%1").arg(QStompClientPrivate::s_subscriptionCounter);
    QStompFrame subscriptionFrame(QStompFrame::SUBSCRIBE);
    subscriptionFrame.setHeader(QStompFrame::IdHeader, uniqueId);
    subscriptionFrame.setHeader(QStompFrame::DestinationHeader, destination);
    subscriptionFrame.setHeader(QStompFrame::AckHeader,
                                QStompSubscriptionPrivate::acknowledgmentModeToString(mode));
    writeFrame(subscriptionFrame);

    QStompSubscription *subscription =
        new QStompSubscription(uniqueId, destination, mode, this);
    d->subscriptions.insert(uniqueId, subscription);
    return subscription;
}

void QStompClient::connectToHost(const QString &host, quint16 port)
{
    Q_D(QStompClient);
    if (d->connected) {
        qDebug() << Q_FUNC_INFO << "already connected";
        return;
    }

    d->stompSocket->connectToHost(host, port);
}

void QStompClient::connectToHost(const QHostAddress &host, quint16 port)
{
    Q_D(QStompClient);
    if (d->connected) {
        qDebug() << Q_FUNC_INFO << "already connected";
        return;
    }

    d->stompSocket->connectToHost(host.toString(), port);
}

void QStompClient::disconnectFromHost()
{
    Q_D(QStompClient);
    if (!d->connected) {
        qWarning() << Q_FUNC_INFO << "not connected";
        return;
    }

    if (d->disconnecting) {
        qWarning() << Q_FUNC_INFO << "already disconnecting";
        return;
    }

    QStompFrame frame(QStompFrame::DISCONNECT);
    // this is absolutely arbitrary at this point
    d->disconnectReceipt = QString("disconnect-%1").arg(qrand() % 5000);
    frame.setHeader(QStompFrame::ReceiptHeader, d->disconnectReceipt);
    writeFrame(frame);
    d->disconnecting = true;
}

void QStompClientPrivate::_q_socketConnected()
{
    Q_Q(QStompClient);
    // prepare STOMP CONNECT frame
    QStompFrame frame(QStompFrame::CONNECT);
    frame.setHeader(QStompFrame::AcceptVersionHeader, "1.0,1.1,1.2");
    frame.setHeader(QStompFrame::HostHeader, stompSocket->peerName());

    if (!login.isEmpty() && !passcode.isEmpty()) {
        frame.setHeader(QStompFrame::LoginHeader, login);
        frame.setHeader(QStompFrame::PasscodeHeader, passcode);
    }

    if (incomingHeartbeatMs > 0 && outgoingHeartbeatMs > 0) {
        QByteArray heartBeatSettings =
                QByteArray::number(incomingHeartbeatMs) + "," +
                QByteArray::number(outgoingHeartbeatMs);
        frame.setHeader(QStompFrame::HeartbeatHeader, heartBeatSettings);
    }

    q->writeFrame(frame);
}

void QStompClientPrivate::_q_socketDisconnected()
{
    // NOTE: should we clean up here as well? undefined behavior spec-wise
    /*
        Q_Q(QStompClient);
        connected = false;
        disconnecting = false;
        disconnectReceipt.clear();
        Q_EMIT q->disconnected();
    */
}

void QStompClientPrivate::_q_socketError(QAbstractSocket::SocketError socketError)
{
    qDebug() << Q_FUNC_INFO << socketError;
}

QByteArray hexDump(const QByteArray &input) {
    QByteArray result;
    QByteArray inputHex = input.toHex();
    for (int i = 0; i < inputHex.size(); i += 2)
        result.append(inputHex.mid(i, 2) + " ");
    return result;
}

bool nextByteIsNull(QIODevice *device)
{
    char buf[1];
    if (device->peek(buf, sizeof(buf)) == sizeof(buf))
        return (buf[0] == '\0');
    return false;
}

void QStompClient::handleFrame(const QStompFrame &frame)
{
    Q_D(QStompClient);
    if (frame.command() == QStompFrame::CONNECTED) {
        d->connected = true;
        Q_EMIT connected(frame);

        // heartbeat management
        if (d->outgoingHeartbeatMs > 0 && frame.hasHeader(QStompFrame::HeartbeatHeader)) {
            QByteArray serverHeartbeat =
                frame.header(QStompFrame::HeartbeatHeader).toByteArray();
            QList<QByteArray> heartbeatParts = serverHeartbeat.split(',');
            if (heartbeatParts.size() > 1) {
                int sy = heartbeatParts.at(1).toInt();
                int msecs = qMax(sy, d->outgoingHeartbeatMs);
                qDebug() << "starting heartbeat timer for: " << msecs << "ms";
                if (d->heartbeatTimer)
                    d->heartbeatTimer->deleteLater();
                d->heartbeatTimer = new QTimer(this);
                connect(d->heartbeatTimer, SIGNAL(timeout()), this, SLOT(_q_sendHeartbeat()));
                d->heartbeatTimer->start(msecs);
            }
        }

    } else if (frame.command() == QStompFrame::ERROR) {
        Q_EMIT error(frame);
    } else if (frame.command() == QStompFrame::RECEIPT) {
        QString receiptId = frame.header(QStompFrame::ReceiptIdHeader).toString();
        Q_EMIT receipt(receiptId);

        if (d->disconnecting && receiptId == d->disconnectReceipt) {
            d->disconnectReceipt.clear();
            d->disconnecting = false;
            d->stompSocket->close();
            d->connected = false;
            Q_EMIT disconnected();
        }
    }
    else {
        if (frame.hasHeader(QStompFrame::IdHeader)) {
            QString subscriptionId = frame.header(QStompFrame::IdHeader).toString();
            if (d->subscriptions.contains(subscriptionId)) {
                QStompSubscription *subscription = d->subscriptions.value(subscriptionId);
                Q_EMIT subscription->message(frame);
            }
        } else {
            Q_EMIT message(frame);
        }
    }
}

void QStompClientPrivate::_q_sendHeartbeat()
{
    if (!connected || !stompSocket->isOpen()) {
        qWarning() << Q_FUNC_INFO << "sending heartbeats when not connected";
        heartbeatTimer->deleteLater();
        return;
    }

    if (stompSocket->bytesToWrite() > 0) {
        // the heartbeat just requires that we send data over the network every
        // n milliseconds. If there is already data to write, there's nothing to
        // worry about here
        return;
    }

    // otherwise, send an EOL
    QTextStream ts(stompSocket);
    ts.setCodec("UTF-8");
    ts << endl;
}

bool QStompClient::waitForConnected(int msecs)
{
    Q_D(QStompClient);
    if (d->connected) {
        qWarning() << Q_FUNC_INFO << "already connected";
        return true;
    }

    QEventLoop loop;
    connect(this, SIGNAL(connected(QStompFrame)), &loop, SLOT(quit()));
    QTimer::singleShot(msecs, &loop, SLOT(quit()));
    loop.exec();

    return d->connected;
}

bool QStompClient::waitForDisconnected(int msecs)
{
    Q_D(QStompClient);
    if (!d->connected) {
        qWarning() << Q_FUNC_INFO << "already disconnected";
        return true;
    }

    QEventLoop loop;
    connect(this, SIGNAL(disconnected()), &loop, SLOT(quit()));
    QTimer::singleShot(msecs, &loop, SLOT(quit()));
    loop.exec();

    return !d->connected;
}

bool QStompClient::waitForAFrame(int msecs)
{
    QStompClientSpy spy(this, this);
    return spy.waitForAFrame(msecs);
}

void QStompClient::send(const QString &destination, const QString &message)
{
    Q_D(QStompClient);
    if (!d->connected) {
        qWarning() << Q_FUNC_INFO << "not connected to anything";
        return;
    }

    QStompFrame sendFrame(QStompFrame::SEND);
    sendFrame.setHeader(QStompFrame::DestinationHeader, destination);
    sendFrame.setHeader(QStompFrame::ContentTypeHeader, QLatin1String("text/plain"));
    sendFrame.setBody(message);
    sendFrame.setHeader(QStompFrame::ContentLengthHeader, sendFrame.rawBody().size() + 1);
    send(sendFrame);
}

void QStompClient::send(const QStompFrame &frame)
{
    Q_D(QStompClient);
    if (!d->connected) {
        qWarning() << Q_FUNC_INFO << "not connected to anything";
        return;
    }

    if (frame.command() != QStompFrame::SEND) {
        qWarning() << Q_FUNC_INFO << "invalid frame command: " << frame.command();
        return;
    }

    writeFrame(frame);
}

#include "moc_qstompclient.cpp"
