#ifndef QSTOMPCLIENT_H
#define QSTOMPCLIENT_H

#include <QObject>
#include <QScopedPointer>
#include <QAbstractSocket>
#include <QHostAddress>

#include "qstompframe.h"
#include "qabstractstompsocket.h"
#include "qstompclient_export.h"

class QStompClient;
class QStompSubscriptionPrivate;
class QSTOMPCLIENT_EXPORT QStompSubscription : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id CONSTANT)
    Q_PROPERTY(QString destination READ destination CONSTANT)
    Q_PROPERTY(QStompSubscription::AcknowledgmentMode mode READ acknowledgmentMode CONSTANT)
    Q_ENUMS(QStompSubscription::AcknowledgmentMode)
public:
    ~QStompSubscription();

    enum AcknowledgmentMode {
        Auto,
        Client,
        ClientIndividual
    };

    AcknowledgmentMode acknowledgmentMode() const;

    QString id() const;
    QString destination() const;

public Q_SLOTS:
    void unsubscribe();

Q_SIGNALS:
    void message(const QStompFrame &message);
    void error(const QStompFrame &error);
    void closed();

private:
    explicit QStompSubscription(const QString &id, const QString &destination,
                                AcknowledgmentMode mode, QStompClient *parent);
    friend class QStompClient;

    Q_DISABLE_COPY(QStompSubscription)
    Q_DECLARE_PRIVATE(QStompSubscription)
    QScopedPointer<QStompSubscriptionPrivate> d_ptr;

};

class QIODevice;
class QStompClientPrivate;
class QSTOMPCLIENT_EXPORT QStompClient : public QAbstractStompSocket
{
    Q_OBJECT
    Q_PROPERTY(QString login READ login WRITE setLogin)
    Q_PROPERTY(QString passcode READ passcode WRITE setPasscode)
    Q_PROPERTY(int outgoinHeartbeatMs READ outgoingHeartbeatMs WRITE setOutgoingHeartbeatMs)
    Q_PROPERTY(int incomingHeartbeatMs READ incomingHeartbeatMs WRITE setIncomingHeartbeatMs)
    Q_PROPERTY(QByteArray session READ session CONSTANT)
public:
    explicit QStompClient(QObject *parent = 0);
    ~QStompClient();

    bool isConnected() const;

    // for CONNECT frame
    QString login() const;
    void setLogin(const QString &login);

    QString passcode() const;
    void setPasscode(const QString &passcode);

    int outgoingHeartbeatMs() const;
    void setOutgoingHeartbeatMs(int msec);

    int incomingHeartbeatMs() const;
    void setIncomingHeartbeatMs(int msec);

    QByteArray session() const;

    // messages
    void send(const QString &destination, const QString &message);
    void send(const QStompFrame &frame);

    QStompSubscription *subscribe(const QString &destination,
        QStompSubscription::AcknowledgmentMode mode = QStompSubscription::Auto);

    virtual void connectToHost(const QString &host, quint16 port = 61613);
    virtual void connectToHost(const QHostAddress &host, quint16 port = 61613);
    virtual void disconnectFromHost();

    // for synchronous access
    bool waitForConnected(int msecs = 30000);
    bool waitForDisconnected(int msecs = 30000);
    bool waitForAFrame(int msecs = 30000);

Q_SIGNALS:
    void connected(const QStompFrame &connectedFrame);
    void error(const QStompFrame &error);
    void receipt(const QString &receiptId);
    void message(const QStompFrame &message);
    void disconnected();

protected:
    QStompClient(QIODevice *socket, QObject *parent = 0);
    virtual void handleFrame(const QStompFrame &frame);

private:
    Q_DISABLE_COPY(QStompClient)
    Q_DECLARE_PRIVATE(QStompClient)
    QScopedPointer<QStompClientPrivate> d_ptr;
    friend class QStompSubscription;

    Q_PRIVATE_SLOT(d_func(), void _q_socketConnected())
    Q_PRIVATE_SLOT(d_func(), void _q_socketDisconnected())
    Q_PRIVATE_SLOT(d_func(), void _q_socketError(QAbstractSocket::SocketError socketError))
    Q_PRIVATE_SLOT(d_func(), void _q_sendHeartbeat())

};

#endif

