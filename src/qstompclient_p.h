#ifndef QSTOMPCLIENT_P_H
#define QSTOMPCLIENT_P_H

#include <QString>
#include <QByteArray>
#include <QEventLoop>
#include <QAbstractSocket>
#include <QQueue>

#include "qstompframe.h"

class QTimer;
class QIODevice;
class QTcpSocket;
class QStompSubscription;
class QStompClient;
class QStompClientPrivate
{
    Q_DECLARE_PUBLIC(QStompClient)

public:
    QStompClientPrivate(QStompClient *qq = 0);
    QString commandName(QStompFrame::Command command) const;

    // private slots
    void _q_socketConnected();
    void _q_socketDisconnected();
    void _q_socketError(QAbstractSocket::SocketError socketError);
    void _q_sendHeartbeat();

    QTcpSocket *stompSocket;
    bool connected;
    QString login;
    QString passcode;
    int incomingHeartbeatMs;
    int outgoingHeartbeatMs;
    QByteArray session;
    QTimer *heartbeatTimer;
    bool disconnecting;
    QString disconnectReceipt;
    QHash<QString, QStompSubscription*> subscriptions;
    static int s_subscriptionCounter;

    QStompClient * const q_ptr;
};

class QStompClientSpy : public QObject, public QQueue<QStompFrame>
{
    Q_OBJECT
public:
    QStompClientSpy(QStompClient *client, QObject *parent);
    QList<QString> receipts() const;
    bool waitForAFrame(int msecs = 30000);

private Q_SLOTS:
    void frameReceived(const QStompFrame &frame);
    void receiptReceived(const QString &receipt);

protected:
    QQueue<QString> m_receipts;
    QEventLoop m_eventLoop;

};

#endif
