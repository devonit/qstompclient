#include <QList>
#include <QPair>
#include <QTextStream>
#include <QDebug>

#include "qstompframe_p.h"
#include "qstompframe.h"

QTextCodec *QStompFramePrivate::s_defaultContentCodec = QTextCodec::codecForName("UTF-8");
QStompFrame::Command QStompFramePrivate::commandFromString(const QString &cmd)
{
    QString command = cmd.toUpper();
    if (command == QLatin1String("SEND"))
        return QStompFrame::SEND;
    else if (command == QLatin1String("SUBSCRIBE"))
        return QStompFrame::SUBSCRIBE;
    else if (command == QLatin1String("UNSUBSCRIBE"))
        return QStompFrame::UNSUBSCRIBE;
    else if (command == QLatin1String("BEGIN"))
        return QStompFrame::BEGIN;
    else if (command == QLatin1String("COMMIT"))
        return QStompFrame::COMMIT;
    else if (command == QLatin1String("ABORT"))
        return QStompFrame::ABORT;
    else if (command == QLatin1String("ACK"))
        return QStompFrame::ACK;
    else if (command == QLatin1String("NACK"))
        return QStompFrame::NACK;
    else if (command == QLatin1String("DISCONNECT"))
        return QStompFrame::DISCONNECT;
    else if (command == QLatin1String("CONNECT"))
        return QStompFrame::CONNECT;
    else if (command == QLatin1String("STOMP"))
        return QStompFrame::STOMP;
    else if (command == QLatin1String("CONNECTED"))
        return QStompFrame::CONNECTED;
    else if (command == QLatin1String("MESSAGE"))
        return QStompFrame::MESSAGE;
    else if (command == QLatin1String("RECEIPT"))
        return QStompFrame::RECEIPT;
    else if (command == QLatin1String("ERROR"))
        return QStompFrame::ERROR;
    return QStompFrame::INVALID;
}

QString QStompFramePrivate::commandToString(QStompFrame::Command command)
{
    switch (command) {
    case QStompFrame::SEND:
        return QLatin1String("SEND");
    case QStompFrame::SUBSCRIBE:
        return QLatin1String("SUBSCRIBE");
    case QStompFrame::UNSUBSCRIBE:
        return QLatin1String("UNSUBSCRIBE");
    case QStompFrame::BEGIN:
        return QLatin1String("BEGIN");
    case QStompFrame::COMMIT:
        return QLatin1String("COMMIT");
    case QStompFrame::ABORT:
        return QLatin1String("ABORT");
    case QStompFrame::ACK:
        return QLatin1String("ACK");
    case QStompFrame::NACK:
        return QLatin1String("NACK");
    case QStompFrame::DISCONNECT:
        return QLatin1String("DISCONNECT");
    case QStompFrame::CONNECT:
        return QLatin1String("CONNECT");
    case QStompFrame::STOMP:
        return QLatin1String("STOMP");
    case QStompFrame::CONNECTED:
        return QLatin1String("CONNECTED");
    case QStompFrame::MESSAGE:
        return QLatin1String("MESSAGE");
    case QStompFrame::RECEIPT:
        return QLatin1String("RECEIPT");
    case QStompFrame::ERROR:
        return QLatin1String("ERROR");
    case QStompFrame::INVALID:
        break;
    }

    return QString();
}

QString QStompFramePrivate::headerName(QStompFrame::KnownHeaders header) const
{
    switch (header) {
    case QStompFrame::AcceptVersionHeader:
        return QLatin1String("accept-version");
    case QStompFrame::AckHeader:
        return QLatin1String("ack");
    case QStompFrame::ContentLengthHeader:
        return QLatin1String("content-length");
    case QStompFrame::ContentTypeHeader:
        return QLatin1String("content-type");
    case QStompFrame::DestinationHeader:
        return QLatin1String("destination");
    case QStompFrame::HeartbeatHeader:
        return QLatin1String("heart-beat");
    case QStompFrame::HostHeader:
        return QLatin1String("host");
    case QStompFrame::IdHeader:
        return QLatin1String("id");
    case QStompFrame::LoginHeader:
        return QLatin1String("login");
    case QStompFrame::MessageHeader:
        return QLatin1String("message");
    case QStompFrame::MessageIdHeader:
        return QLatin1String("message-id");
    case QStompFrame::PasscodeHeader:
        return QLatin1String("passcode");
    case QStompFrame::ReceiptHeader:
        return QLatin1String("receipt");
    case QStompFrame::ReceiptIdHeader:
        return QLatin1String("receipt-id");
    case QStompFrame::SessionHeader:
        return QLatin1String("session");
    case QStompFrame::ServerHeader:
        return QLatin1String("server");
    case QStompFrame::SubscriptionHeader:
        return QLatin1String("subscription");
    case QStompFrame::TransactionHeader:
        return QLatin1String("transaction");
    case QStompFrame::VersionHeader:
        return QLatin1String("version");
    }

    return QString();
}

bool QStompFramePrivate::hasRequiredHeaders() const
{
    switch (command) {
    case QStompFrame::INVALID:
        return false;
    case QStompFrame::SEND: {
        if (!hasHeader(QStompFrame::DestinationHeader))
            return false;

        if (!body.isEmpty()) {
            // from spec: SEND frames SHOULD include a content-length header
            //            and a content-type header if a body is present.
            //
            // just warn here
            if (!hasHeader(QStompFrame::ContentLengthHeader))
                qWarning() << Q_FUNC_INFO << "missing header: "
                           << headerName(QStompFrame::ContentLengthHeader);
            if (!hasHeader(QStompFrame::ContentTypeHeader))
                qWarning() << Q_FUNC_INFO << "missing header: "
                           << headerName(QStompFrame::ContentLengthHeader);
        }

        return true;
    }
    case QStompFrame::SUBSCRIBE:
        return hasHeader(QStompFrame::DestinationHeader) &&
               hasHeader(QStompFrame::IdHeader);
    case QStompFrame::UNSUBSCRIBE:
        return hasHeader(QStompFrame::IdHeader);
    case QStompFrame::BEGIN:
    case QStompFrame::COMMIT:
    case QStompFrame::ABORT:
        return hasHeader(QStompFrame::TransactionHeader);
    case QStompFrame::ACK:
    case QStompFrame::NACK:
        return hasHeader(QStompFrame::IdHeader);
    case QStompFrame::STOMP:
    case QStompFrame::CONNECT:
        return hasHeader(QStompFrame::AcceptVersionHeader) &&
               hasHeader(QStompFrame::HostHeader);
    case QStompFrame::CONNECTED:
        return hasHeader(QStompFrame::VersionHeader);
    case QStompFrame::MESSAGE:
        return hasHeader(QStompFrame::DestinationHeader) &&
               hasHeader(QStompFrame::MessageIdHeader);
// NOTE: not clear in the spec if this is required
//               && hasHeader(QStompFrame::SubscriptionHeader);
    case QStompFrame::RECEIPT:
        return hasHeader(QStompFrame::ReceiptIdHeader);

    case QStompFrame::ERROR:
    case QStompFrame::DISCONNECT:
        break;
    }

    return true;
}

QStompFrame::QStompFrame()
    : d(new QStompFramePrivate(this))
{
    d->command = INVALID;
}

QStompFrame::QStompFrame(Command command)
    : d(new QStompFramePrivate(this))
{
    d->command = command;
}

QStompFrame::QStompFrame(const QStompFrame &other)
    : d(other.d)
{
}

QStompFrame::QStompFrame(const QString &cmd)
    : d(new QStompFramePrivate(this))
{
    d->command = QStompFramePrivate::commandFromString(cmd);
}

QStompFrame::~QStompFrame()
{
}

QStompFrame &QStompFrame::operator=(const QStompFrame &other)
{
    d = other.d;
    return *this;
}

bool QStompFrame::operator==(const QStompFrame &other) const
{
    return d == other.d || *d == *other.d;
}

QStompFrame QStompFrame::createError(const QString &message, const QString &body) const
{
    QStompFrame result(QStompFrame::ERROR);
    if (hasHeader(QStompFrame::ReceiptHeader))
        result.setHeader(QStompFrame::ReceiptIdHeader, header(QStompFrame::ReceiptHeader));
    result.setHeader(QStompFrame::MessageHeader, message);
    if (!body.isEmpty()) {
        result.setHeader(QStompFrame::ContentTypeHeader, QLatin1String("text/plain"));
        result.setBody(body);
        result.setHeader(QStompFrame::ContentLengthHeader, result.rawBody().size() + 1);
    }

    return result;
}

bool QStompFrame::hasHeader(KnownHeaders header) const
{
    return d->hasHeader(header);
}

QVariant QStompFrame::header(KnownHeaders header) const
{
    if (!d->headers.contains(header)) {
        if (header == QStompFrame::ContentLengthHeader)
            return rawHeader(d->headerName(header)).toInt();
        return rawHeader(d->headerName(header));
    }

    if (header == QStompFrame::ContentLengthHeader)
        return d->headers.value(header).toInt();
    return d->headers.value(header);
}

void QStompFrame::setHeader(KnownHeaders header, const QVariant &value)
{
    d->headers.insert(header, value);
    setRawHeader(d->headerName(header), value.toByteArray());
}

QString QStompFrame::rawHeader(const QString &header) const
{
    QStompFramePrivate::RawStompHeaders::ConstIterator it =
            d->findHeader(header);
    if (it != d->rawHeaders.constEnd())
        return it->second;
    return QString();
}

void QStompFrame::setRawHeader(const QString &header, const QString &value)
{
    QStompFramePrivate::RawStompHeaders::Iterator it = d->rawHeaders.begin();
    while (it != d->rawHeaders.end()) {
        if (qstricmp(it->first.toUtf8().constData(), header.toUtf8().constData()) == 0)
            it = d->rawHeaders.erase(it);
        else
            ++it;
    }

    d->rawHeaders.append(qMakePair(header, value));

    // NOTE: this is needed for qt5 and it proves that the tests are not ideal
    //       as they require order. Fix this later, and remove this extra step
    //       since it adds overhead.
    std::sort(d->rawHeaders.begin(), d->rawHeaders.end());
}

QStompFrame::Command QStompFrame::command() const
{
    return d->command;
}

QString QStompFrame::body() const
{
    return d->contentCodec->toUnicode(d->body);
}

void QStompFrame::setBody(const QString &body)
{
    d->body = d->contentCodec->fromUnicode(body);
}

QByteArray QStompFrame::rawBody() const
{
    return d->body;
}

bool QStompFrame::isValid() const
{
    if ((d->command != QStompFrame::SEND &&
         d->command != QStompFrame::ERROR &&
         d->command != QStompFrame::MESSAGE) && !d->body.isEmpty())
        return false;
    return (d->command != QStompFrame::INVALID) && d->hasRequiredHeaders();
}

QTextCodec *QStompFrame::contentTypeEncoding() const
{
    return d->contentCodec;
}

void QStompFrame::setContentTypeEncoding(QTextCodec *encoding)
{
    d->contentCodec = encoding;
}

void QStompFrame::setContentTypeEncoding(const QByteArray &encoding)
{
    d->contentCodec = QTextCodec::codecForName(encoding);
}

QTextCodec *QStompFrame::defaultContentTypeEncoding()
{
    return QStompFramePrivate::s_defaultContentCodec;
}

void QStompFrame::setDefaultContentTypeEncoding(QTextCodec *encoding)
{
    QStompFramePrivate::s_defaultContentCodec = encoding;
}

void QStompFrame::setDefaultContentTypeEncoding(const QByteArray &encoding)
{
    QStompFramePrivate::s_defaultContentCodec = QTextCodec::codecForName(encoding);
}

QDebug operator<<(QDebug dbg, const QStompFrame &frame)
{
    dbg.nospace() << "QStompFrame(command=" << QStompFramePrivate::commandToString(frame.command());
/*
    if (msg.type() != QJsonRpcMessage::Notification) {
    dbg.nospace() << ", id=" << msg.id();
    }

    if (msg.type() == QJsonRpcMessage::Request ||
    msg.type() == QJsonRpcMessage::Notification) {
    dbg.nospace() << ", method=" << msg.method()
                      << ", params=" << msg.params();
    } else if (msg.type() == QJsonRpcMessage::Response) {
    dbg.nospace() << ", result=" << msg.result();
    } else if (msg.type() == QJsonRpcMessage::Error) {
    dbg.nospace() << ", code=" << msg.errorCode()
                      << ", message=" << msg.errorMessage()
                      << ", data=" << msg.errorData();
    }
*/
    dbg.nospace() << ")";
    return dbg.space();
}
