#ifndef QSTOMPFRAME_H
#define QSTOMPFRAME_H

#include <QList>
#include <QByteArray>
#include <QVariant>
#include <QHash>
#include <QSharedDataPointer>

#include "qstompclient_export.h"

/*
 * STOMP is a frame based protocol, with frames modelled on HTTP.
 *A frame consists of:
 *  a command,
 *  a set of optional headers
 *  an optional body.
 *
 * STOMP is text based but also allows for the transmission of binary messages.
 * The default encoding for STOMP is UTF-8, but it supports the specification
 * of alternative encodings for message bodies.
 */

class QAbstractStompSocketPrivate;
class QStompFramePrivate;
class QSTOMPCLIENT_EXPORT QStompFrame
{
public:
    enum Command
    {
        // default
        INVALID = -1,

        // client command
        SEND,
        SUBSCRIBE,
        UNSUBSCRIBE,
        BEGIN,
        COMMIT,
        ABORT,
        ACK,
        NACK,
        DISCONNECT,
        CONNECT,
        STOMP,

        // server command
        CONNECTED,
        MESSAGE,
        RECEIPT,
        ERROR
    };

    enum KnownHeaders
    {
        AcceptVersionHeader,
        AckHeader,
        ContentLengthHeader,
        ContentTypeHeader,
        DestinationHeader,
        HeartbeatHeader,
        HostHeader,
        IdHeader,
        LoginHeader,
        MessageHeader,
        MessageIdHeader,
        PasscodeHeader,
        ReceiptHeader,
        ReceiptIdHeader,
        SessionHeader,
        ServerHeader,
        SubscriptionHeader,
        TransactionHeader,
        VersionHeader
    };

public:
    QStompFrame();
    QStompFrame(Command command);
    QStompFrame(const QString &command);
    QStompFrame(const QStompFrame &other);
    virtual ~QStompFrame();

    QStompFrame &operator=(const QStompFrame &other);

    bool operator==(const QStompFrame &other) const;
    inline bool operator!=(const QStompFrame &other) const {
        return !operator==(other);
    }

    QStompFrame createError(const QString &message, const QString &body = QString()) const;

    bool hasHeader(KnownHeaders header) const;
    QVariant header(KnownHeaders header) const;
    void setHeader(KnownHeaders header, const QVariant &value);

    // NOTE: headers are always utf-8 encoded
    bool hasRawHeader(const QString &header) const;
    QString rawHeader(const QString &header) const;
    void setRawHeader(const QString &header, const QString &value);

    Command command() const;

    QString body() const;
    void setBody(const QString &body);
    QByteArray rawBody() const;

    bool isValid() const;

    QTextCodec *contentTypeEncoding() const;
    void setContentTypeEncoding(QTextCodec *encoding);
    void setContentTypeEncoding(const QByteArray &encoding);

    static QTextCodec *defaultContentTypeEncoding();
    static void setDefaultContentTypeEncoding(QTextCodec *encoding);
    static void setDefaultContentTypeEncoding(const QByteArray &encoding);

private:
    friend class QAbstractStompSocket;
    friend class QAbstractStompSocketPrivate;
    QSharedDataPointer<QStompFramePrivate> d;

};

QSTOMPCLIENT_EXPORT QDebug operator<<(QDebug, const QStompFrame &);
Q_DECLARE_METATYPE(QStompFrame)

#endif
