#ifndef QSTOMPFRAME_P_H
#define QSTOMPFRAME_P_H

#include <QSharedData>
#include <QList>
#include <QPair>
#include <QTextCodec>

#include "qstompframe.h"

class QStompFramePrivate : public QSharedData
{
public:
    typedef QHash<QStompFrame::KnownHeaders, QVariant> StompHeaders;
    typedef QList< QPair<QString, QString> > RawStompHeaders;

    QStompFramePrivate(QStompFrame *parent)
        : command(QStompFrame::INVALID),
          contentCodec(s_defaultContentCodec),
          q(parent)
    {
    }

    inline bool operator==(const QStompFramePrivate &other) const {
        return headers == other.headers &&
               command == other.command &&
               body == other.body;
    }

    bool hasHeader(QStompFrame::KnownHeaders header) const {
        return headers.contains(header) || hasHeader(headerName(header));
    }

    bool hasHeader(const QString &header) const {
        return findHeader(header) != rawHeaders.constEnd();
    }

    RawStompHeaders::ConstIterator findHeader(const QString &header) const {
        RawStompHeaders::ConstIterator it = rawHeaders.constBegin();
        RawStompHeaders::ConstIterator end = rawHeaders.constEnd();
        for (; it != end; ++it) {
            if (qstricmp(it->first.toUtf8().constData(), header.toUtf8().constData()) == 0)
                return it;
        }

        return end;
    }

    static QStompFrame::Command commandFromString(const QString &command);
    static QString commandToString(QStompFrame::Command command);

    bool hasRequiredHeaders() const;
    QString headerName(QStompFrame::KnownHeaders header) const;

    StompHeaders headers;
    RawStompHeaders rawHeaders;
    QStompFrame::Command command;
    QByteArray body;
    QTextCodec *contentCodec;
    static QTextCodec *s_defaultContentCodec;

private:
    QStompFrame * const q;

};

#endif
