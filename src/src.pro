include(../qstompclient.pri)

INCLUDEPATH += .
TEMPLATE = lib
TARGET = qstompclient
QT += core network
QT -= gui
DEFINES += QSTOMPCLIENT_BUILD
CONFIG += $${QSTOMPCLIENT_LIBRARY_TYPE}
VERSION = $${QSTOMPCLIENT_VERSION}
win32:DESTDIR = $$OUT_PWD

PRIVATE_HEADERS += \
    qstompframe_p.h \
    qstompclient_p.h

INSTALL_HEADERS += \
    qstompframe.h \
    qabstractstompsocket.h \
    qstompclient.h \
    qstompclient_export.h

HEADERS += \
    $${INSTALL_HEADERS} \
    $${PRIVATE_HEADERS}

SOURCES += \
    qstompframe.cpp \
    qabstractstompsocket.cpp \
    qstompclient.cpp

# install
headers.files = $${INSTALL_HEADERS}
headers.path = $${PREFIX}/include/qstompclient
target.path = $${PREFIX}/$${LIBDIR}
INSTALLS += headers target

# pkg-config support
CONFIG += create_pc create_prl no_install_prl
QMAKE_PKGCONFIG_DESTDIR = pkgconfig
QMAKE_PKGCONFIG_LIBDIR = $$target.path
QMAKE_PKGCONFIG_INCDIR = $$headers.path
equals(QSTOMPCLIENT_LIBRARY_TYPE, staticlib) {
    QMAKE_PKGCONFIG_CFLAGS = -DQSTOMPCLIENT_STATIC
} else {
    QMAKE_PKGCONFIG_CFLAGS = -DQSTOMPCLIENT_SHARED
}
unix:QMAKE_CLEAN += -r pkgconfig lib$${TARGET}.prl
