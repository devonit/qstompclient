#include <QTcpSocket>

#include "fakestompserver.h"

FakeStompServerSocket::FakeStompServerSocket(qintptr fd, QTcpServer *parent)
    : QAbstractStompSocket(parent),
      m_socket(0)
{
    m_socket = new QTcpSocket(this);
    m_socket->setSocketDescriptor(fd);
    setDevice(m_socket);
}

void FakeStompServerSocket::handleFrame(const QStompFrame &frame)
{
    if (!frame.isValid()) {
        QStompFrame errorFrame = frame.createError("invalid headers");
        writeFrame(errorFrame);
        return;
    }

    switch (frame.command()) {
    case QStompFrame::CONNECT: {
        QStompFrame connectedFrame(QStompFrame::CONNECTED);
        connectedFrame.setHeader(QStompFrame::ServerHeader, "FakeQStompServer/1.0");
        connectedFrame.setHeader(QStompFrame::VersionHeader, "1.2");
        connectedFrame.setHeader(QStompFrame::HeartbeatHeader, "0,0");
        m_session = "session-" + QByteArray::number(qrand() % 1000);
        connectedFrame.setHeader(QStompFrame::SessionHeader, m_session);
        writeFrame(connectedFrame);
    }
        break;

    case QStompFrame::DISCONNECT: {
        QStompFrame disconnectedFrame(QStompFrame::RECEIPT);
        disconnectedFrame.setHeader(QStompFrame::ReceiptIdHeader, frame.header(QStompFrame::ReceiptHeader));
        writeFrame(disconnectedFrame);
    }
        break;

    default:
        break;
    }

}

FakeStompServer::FakeStompServer(QObject *parent)
    : QTcpServer(parent)
{
}

FakeStompServer::~FakeStompServer()
{
    qDeleteAll(m_sockets);
    m_sockets.clear();
}

void FakeStompServer::incomingConnection(qintptr handle)
{
    FakeStompServerSocket *socket = new FakeStompServerSocket(handle, this);
    // connect(socket, SIGNAL(disconnected()), this, SLOT(_q_socketDisconnected()));
    m_sockets.append(socket);
}

void FakeStompServer::_q_socketDisconnected()
{
    FakeStompServerSocket *socket = qobject_cast<FakeStompServerSocket*>(sender());
    if (!socket) {
        qWarning() << Q_FUNC_INFO << "invalid socket";
        return;
    }

    m_sockets.removeAll(socket);
    socket->deleteLater();
}
