#ifndef FAKESTOMPSERVER_H
#define FAKESTOMPSERVER_H

#include <QTcpServer>
#include "qabstractstompsocket.h"

class QTcpSocket;
class FakeStompServerSocket : public QAbstractStompSocket
{
    Q_OBJECT
public:
    explicit FakeStompServerSocket(qintptr fd, QTcpServer *parent);

protected:
    virtual void handleFrame(const QStompFrame &frame);

private:
    QTcpSocket *m_socket;
    QString m_session;

};

class FakeStompServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit FakeStompServer(QObject *parent = 0);
    ~FakeStompServer();

protected:
    virtual void incomingConnection(qintptr handle);

private Q_SLOTS:
    void _q_socketDisconnected();

private:
    QList<FakeStompServerSocket*> m_sockets;

};

#endif // FAKESTOMPSERVER_H
