DEPTH = ../../..
include($${DEPTH}/qstompclient.pri)
include($${DEPTH}/tests/tests.pri)

TARGET = tst_qstompclient
SOURCES += tst_qstompclient.cpp \
    fakestompserver.cpp

HEADERS += \
    fakestompserver.h
