#include <QtTest/QtTest>

#include "qstompclient.h"
#include "fakestompserver.h"

#define TEST_PORT 61614

class tst_QStompClient : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();

    void connect();
    void disconnect();
    void subscribe();

private:
    FakeStompServer m_server;

};

void tst_QStompClient::initTestCase()
{
    QVERIFY(m_server.listen(QHostAddress::LocalHost, TEST_PORT));
}

void tst_QStompClient::cleanupTestCase()
{
    m_server.close();
}

void tst_QStompClient::connect()
{
    QStompClient client;
    client.connectToHost(QHostAddress::LocalHost, TEST_PORT);
    QVERIFY(client.waitForConnected());
}

void tst_QStompClient::disconnect()
{
    QStompClient client;
    client.connectToHost(QHostAddress::LocalHost, TEST_PORT);
    QVERIFY(client.waitForConnected());
    client.disconnectFromHost();
    QVERIFY(client.waitForDisconnected());
}

void tst_QStompClient::subscribe()
{
    QStompClient client;
    client.connectToHost(QHostAddress::LocalHost, TEST_PORT);
    QVERIFY(client.waitForConnected());

    QScopedPointer<QStompSubscription> subscription(client.subscribe("/queue/a"));
    QTestEventLoop::instance().connect(subscription.data(),
                                       SIGNAL(message(QStompFrame)), SLOT(exitLoop()));
    client.send("/queue/a", "testData");
    QTestEventLoop::instance().enterLoop(1);
    QVERIFY(!QTestEventLoop::instance().timeout());

    client.disconnectFromHost();
    QVERIFY(client.waitForDisconnected());
}

QTEST_GUILESS_MAIN(tst_QStompClient)
#include "tst_qstompclient.moc"
