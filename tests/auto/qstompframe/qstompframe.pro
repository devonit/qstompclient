DEPTH = ../../..
include($${DEPTH}/qstompclient.pri)
include($${DEPTH}/tests/tests.pri)

TARGET = tst_qstompframe
SOURCES += tst_qstompframe.cpp
RESOURCES = testframes.qrc
