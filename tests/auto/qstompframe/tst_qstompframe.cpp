#include <QtTest/QtTest>
#include <QBuffer>
#include <QQueue>
#include <QSignalSpy>
#include <QDebug>

#include "qstompclient.h"
#include "qstompclient_p.h"
#include "qstompframe_p.h"
#include "qstompframe.h"

typedef QHash<QString, QString> ExtraHeaders;
Q_DECLARE_METATYPE(ExtraHeaders)
Q_DECLARE_METATYPE(QStompFramePrivate::StompHeaders)
Q_DECLARE_METATYPE(QStompFrame::Command)

class QStompClientListener : public QStompClientSpy
{
    Q_OBJECT
public:
    QStompClientListener(QStompClient *client, QObject *parent = 0)
        : QStompClientSpy(client, parent),
          m_spy(new QSignalSpy(client, SIGNAL(message(QStompFrame))))
    {
    }

    bool hasFrame() const {
        return !isEmpty();
    }

    bool hasReceipt() const {
        return !m_receipts.isEmpty();
    }

    int frameCount() const {
        return size();
    }

    int receiptCount() const {
        return m_receipts.count();
    }

    void waitForMessages(int count, int msec = 5000) {
        QElapsedTimer timer;
        timer.start();

        forever {
            m_spy->wait(msec);
            if (m_spy->count() == count)
                break;
            if (timer.elapsed() == msec)
                break;
        }
    }

    QStompFrame lastFrame() { return dequeue(); }
    QString lastReceipt() { return m_receipts.dequeue(); }

private:
    QSignalSpy *m_spy;

};

class FakeQStompClient : public QStompClient
{
    Q_OBJECT
public:
    FakeQStompClient(QBuffer *buffer, QObject *parent = 0)
        : QStompClient(buffer, parent),
          m_buffer(buffer)
    {
    }

    ~FakeQStompClient() { m_buffer->deleteLater(); }

    void writeData(const QByteArray &data) {
        m_buffer->write(data);
        m_buffer->seek(0);
    }

public Q_SLOTS:
    virtual void connectToHost(const QString &host, quint16 port = 61613)
    {
        Q_UNUSED(host)
        Q_UNUSED(port)
        QVERIFY(m_buffer->open(QIODevice::ReadWrite));
    }

    virtual void disconnectFromHost()
    {
        if (m_buffer->isOpen())
            m_buffer->close();
    }

private:
    QBuffer *m_buffer;

};

class tst_QStompFrame : public QObject
{
    Q_OBJECT
private slots:

    // client frames
    void writeMessage_data();
    void writeMessage();

    // server frames
    void multipleMessages();
    void message_data();
    void message();

private:
    QByteArray hexDump(const QByteArray &input) {
        QByteArray result;
        QByteArray inputHex = input.toHex();
        for (int i = 0; i < inputHex.size(); i += 2)
            result.append(inputHex.mid(i, 2) + " ");
        return result;
    }

};

void tst_QStompFrame::writeMessage_data()
{
    QTest::addColumn<QString>("file");
    QTest::addColumn<QStompFramePrivate::StompHeaders>("knownHeaders");
    QTest::addColumn<ExtraHeaders>("extraHeaders");
    QTest::addColumn<QString>("body");
    QTest::addColumn<bool>("valid");

    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::DestinationHeader, "/queue/test");
        headers.insert(QStompFrame::MessageIdHeader, QString::fromUtf8("msg-§54321"));
        headers.insert(QStompFrame::ContentLengthHeader, 13);
        headers.insert(QStompFrame::ContentTypeHeader, "text/plain");
        QTest::newRow("message1") << "msg1-1.1.txt"
            << headers
            << ExtraHeaders()
            << "hello world!"
            << true;
    }

    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::DestinationHeader, "/queue/test");
        headers.insert(QStompFrame::MessageIdHeader, "msg-54321");
        QTest::newRow("message3") << "msg3.txt"
            << headers
            << ExtraHeaders()
            << "hello world!"
            << true;
    }

    /*
    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::DestinationHeader, "/queue/test");
        headers.insert(QStompFrame::MessageIdHeader, "msg-54321");
        headers.insert(QStompFrame::ContentLengthHeader, 23);
        QTest::newRow("message2") << "msg2.txt"
            << headers
            << ExtraHeaders()
            << "hello \0\0\0\0\0\0\0\0  world!"
            << true;
    }

    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::DestinationHeader, "/queue/test");
        headers.insert(QStompFrame::MessageIdHeader, "msg-54321");
        headers.insert(QStompFrame::ContentLengthHeader, 22);
        QTest::newRow("message5") << "msg5.txt"
            << headers
            << QVariantMap()
            << "hello ^@^@^@^@^@^@^@^@  world!"
            << true;
    }
    */

    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::DestinationHeader, "/queue/test");
        headers.insert(QStompFrame::MessageIdHeader, "msg-54321");
        headers.insert(QStompFrame::ContentLengthHeader, 13);
        headers.insert(QStompFrame::ContentTypeHeader, "text/plain");

        ExtraHeaders extraHeaders;
        extraHeaders.insert("special1", "xyz");
        extraHeaders.insert("special2", "123");

        QTest::newRow("message6") << "msg7-1.1.txt"
            << headers
            << extraHeaders
            << "hello world!"
            << true;
    }

    /*
    QTest::newRow("message1") << "msg1-1.1.txt" << QStompFrame::MESSAGE;
    QTest::newRow("message2") << "msg2.txt" << QStompFrame::MESSAGE;
    QTest::newRow("message3") << "msg3.txt" << QStompFrame::MESSAGE;
    QTest::newRow("message4") << "msg4.txt" << QStompFrame::MESSAGE;
    QTest::newRow("message5") << "msg5.txt" << QStompFrame::MESSAGE;
    QTest::newRow("message6") << "msg7-1.1.txt" << QStompFrame::MESSAGE;
    QTest::newRow("send1") << "send1-1.1.txt" << QStompFrame::SEND;
    QTest::newRow("send2") << "send2-1.1.txt" << QStompFrame::SEND;
    QTest::newRow("send3") << "send2.txt" << QStompFrame::SEND;
    QTest::newRow("send4") << "send3.txt" << QStompFrame::SEND;
    QTest::newRow("send5") << "send4.txt" << QStompFrame::SEND;
    QTest::newRow("send6") << "send5.txt" << QStompFrame::SEND;
    QTest::newRow("send7") << "send6-1.1.txt" << QStompFrame::SEND;
    QTest::newRow("send8") << "send7-1.1.txt" << QStompFrame::SEND;
    QTest::newRow("subscribe1") << "sub1-1.1.txt" << QStompFrame::SUBSCRIBE;
    QTest::newRow("subscribe2") << "sub1.txt" << QStompFrame::SUBSCRIBE;
    QTest::newRow("subscribe3") << "sub2-1.1.txt" << QStompFrame::SUBSCRIBE;
    QTest::newRow("subscribe4") << "sub3-1.1.txt" << QStompFrame::SUBSCRIBE;
    QTest::newRow("subscribe5") << "sub4-1.1.txt" << QStompFrame::SUBSCRIBE;
    */
}

class FakeStompSocket : public QAbstractStompSocket
{
public:
    FakeStompSocket(QBuffer *buffer, QObject *parent = 0)
        : QAbstractStompSocket(buffer, parent)
    {
    }

    void writeStompFrame(const QStompFrame &frame) { QAbstractStompSocket::writeFrame(frame); }

protected:
    virtual void handleFrame(const QStompFrame &frame) {
        Q_UNUSED(frame);
    }
};

void tst_QStompFrame::writeMessage()
{
    QFETCH(QString, file);
    QFETCH(QStompFramePrivate::StompHeaders, knownHeaders);
    QFETCH(ExtraHeaders, extraHeaders);
    QFETCH(QString, body);
    // QFETCH(bool, valid);

    // build the frame
    QStompFrame frame(QStompFrame::MESSAGE);
    QStompFramePrivate::StompHeaders::ConstIterator it = knownHeaders.constBegin();
    for (; it != knownHeaders.constEnd(); ++it)
        frame.setHeader(it.key(), it.value());
    if (!extraHeaders.isEmpty()) {
        foreach (QString extraHeader, extraHeaders.keys()) {
            QString value = extraHeaders.value(extraHeader);
            frame.setRawHeader(extraHeader, value);
        }
    }

    if (!body.isEmpty()) {
        frame.setBody(body);
    }

    // write the frame to a buffer
    QBuffer buffer;
    QVERIFY(buffer.open(QIODevice::ReadWrite));
    FakeStompSocket client(&buffer);
    client.writeStompFrame(frame);

    // compare
    QFile testFile(":/files/" + file);
    QVERIFY(testFile.open(QIODevice::ReadOnly));
    QTextStream in(&testFile);
    in.setCodec("UTF-8");

    QByteArray fileFrame = in.readAll().toUtf8();
    QByteArray bufferFrame = buffer.buffer();

//    qDebug() << " fromFile: " << hexDump(fileFrame);
//    qDebug() << "generated: " << hexDump(bufferFrame);

    QCOMPARE(fileFrame, bufferFrame);
}

void tst_QStompFrame::multipleMessages()
{
    FakeQStompClient client(new QBuffer);
    QStompClientListener listener(&client);
    client.connectToHost(QString());

    // prepare frames
    QStringList messageFiles;
    messageFiles << "msg1-1.1.txt" << "msg2.txt" << "msg3.txt" << "msg5.txt";
    QList<QByteArray> messages;
    foreach (QString messageFile, messageFiles) {
        QFile testFile(":/files/" + messageFile);
        QVERIFY(testFile.open(QIODevice::ReadOnly));
        QTextStream in(&testFile);
        in.setCodec("UTF-8");
        messages << in.readAll().toUtf8();
        testFile.close();
    }

    QByteArray data;
    int messageCount = 1000;
    for (int i = 0; i < messageCount; ++i)
        data.append(messages.at(qrand() % messages.size()));

    client.writeData(data);
    listener.waitForMessages(messageCount);
    QCOMPARE(listener.frameCount(), messageCount);
}

void tst_QStompFrame::message_data()
{
    QTest::addColumn<QString>("file");
    QTest::addColumn<QStompFrame::Command>("command");
    QTest::addColumn<QStompFramePrivate::StompHeaders>("knownHeaders");
    QTest::addColumn<ExtraHeaders>("extraHeaders");
    QTest::addColumn<QString>("body");

    QTextCodec *codec = QStompFrame::defaultContentTypeEncoding();
    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::ContentLengthHeader, 13);
        headers.insert(QStompFrame::ContentTypeHeader, "text/plain");
        headers.insert(QStompFrame::DestinationHeader, "/queue/test");
        headers.insert(QStompFrame::MessageIdHeader, QString::fromUtf8("msg-§54321"));

        QTest::newRow("message1") << "msg1-1.1.txt"
            << QStompFrame::MESSAGE
            << headers
            << ExtraHeaders()
            << "hello world!\n";
    }

    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::ContentLengthHeader, 23);
        headers.insert(QStompFrame::DestinationHeader, "/queue/test");
        headers.insert(QStompFrame::MessageIdHeader, "msg-54321");

        QTest::newRow("message2") << "msg2.txt"
            << QStompFrame::MESSAGE
            << headers
            << ExtraHeaders()
            << codec->toUnicode(QByteArray::fromHex("68656c6c6f2000000000000000002020776f726c64210a"));
    }

    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::DestinationHeader, "/queue/test");
        headers.insert(QStompFrame::MessageIdHeader, "msg-54321");

        QTest::newRow("message3") << "msg3.txt"
            << QStompFrame::MESSAGE
            << headers
            << ExtraHeaders()
            << "hello world!\n";
    }

    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::DestinationHeader, "/queue/test");
        headers.insert(QStompFrame::MessageIdHeader, "msg-54321");
        headers.insert(QStompFrame::ContentLengthHeader, 22);

        QTest::newRow("message5") << "msg5.txt"
            << QStompFrame::MESSAGE
            << headers
            << ExtraHeaders()
            << codec->toUnicode(QByteArray::fromHex("68656c6c6f2000000000000000002020776f726c6421"));
    }

    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::VersionHeader, "1.1");
        headers.insert(QStompFrame::HeartbeatHeader, "500, 500");
        QTest::newRow("connected1") << "cond-1.1.txt"
            << QStompFrame::CONNECTED
            << headers
            << ExtraHeaders()
            << QString();
    }

    {
        QStompFramePrivate::StompHeaders headers;
        headers.insert(QStompFrame::ContentTypeHeader, "text/plain");

        ExtraHeaders extraHeaders;
        extraHeaders.insert("message", "Malformed package received");

        QTest::newRow("error1") << "err1-1.1.txt"
            << QStompFrame::ERROR
            << headers
            << extraHeaders
            << QString();
    }
}

void tst_QStompFrame::message()
{
    QFETCH(QString, file);
    QFETCH(QStompFrame::Command, command);
    QFETCH(QStompFramePrivate::StompHeaders, knownHeaders);
    QFETCH(QString, body);

    FakeQStompClient client(new QBuffer);
    QStompClientListener listener(&client);
    client.connectToHost(QString());

    // write the data
    QFile testFile(":/files/" + file);
    QVERIFY(testFile.open(QIODevice::ReadOnly));
    QTextStream in(&testFile);
    in.setCodec("UTF-8");
    QByteArray data = in.readAll().toUtf8();
    client.writeData(data);

    // get the frame and check data
    QVERIFY(listener.waitForAFrame());
    QVERIFY(listener.hasFrame());

    QStompFrame frame = listener.lastFrame();
    QCOMPARE(frame.command(), command);
    foreach (QStompFrame::KnownHeaders header, knownHeaders.keys()) {
        QVERIFY(frame.hasHeader(header));
        QCOMPARE(frame.header(header), knownHeaders.value(header));
    }

    if (frame.hasHeader(QStompFrame::ContentLengthHeader))
        QCOMPARE(frame.header(QStompFrame::ContentLengthHeader).toInt(),
                 frame.rawBody().size());

    if (!body.isEmpty() || !body.isNull())
        QCOMPARE(frame.body(), body);
}

QTEST_GUILESS_MAIN(tst_QStompFrame)
#include "tst_qstompframe.moc"
