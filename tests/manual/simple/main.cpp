#include <QCoreApplication>

#include "qstompclient.h"

class Listener : public QObject
{
    Q_OBJECT
public:
    Listener(QStompClient *client, QObject *parent = 0)
        : QObject(parent),
          m_client(client)
    {
        connect(m_client, SIGNAL(connected(QStompFrame)), this, SLOT(clientConnected(QStompFrame)));
        connect(m_client, SIGNAL(error(QStompFrame)), this, SLOT(clientError(QStompFrame)));
        connect(m_client, SIGNAL(message(QStompFrame)), this, SLOT(clientMessage(QStompFrame)));
        connect(m_client, SIGNAL(receipt(QString)), this, SLOT(clientReceipt(QString)));
    }

private Q_SLOTS:
    void clientConnected(const QStompFrame &frame) {
        Q_UNUSED(frame)
        qDebug() << Q_FUNC_INFO;
    }

    void clientError(const QStompFrame &frame) {
        Q_UNUSED(frame)
        qDebug() << Q_FUNC_INFO << frame.body();
    }

    void clientMessage(const QStompFrame &frame) {
        Q_UNUSED(frame)
        qDebug() << Q_FUNC_INFO;
    }

    void clientReceipt(const QString &receipt) {
        qDebug() << Q_FUNC_INFO << receipt;
    }

private:
    QStompClient *m_client;

};

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QStompClient client;
    Listener listener(&client);
    client.setLogin("guest");
    client.setPasscode("guest");
    client.connectToHost(QHostAddress::LocalHost, 61613);
    if (!client.waitForConnected()) {
        qDebug() << "couldn't connect to message bus";
        return -1;
    }

    client.disconnectFromHost();
    if (!client.waitForDisconnected()) {
        qDebug() << "couldn't disconnect from message bus";
        return -1;
    }

    return 0;
}

#include "main.moc"
